import React from "react";
import "./MainScreen.css";
import ButtonBar from "./ButtonBar";
import {Input} from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import {Button} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";

interface Chat {
    type: "user" | "receiver";
    message: string;
}

interface State {
    chatHistory: Chat[];
    waiting: boolean
    showHeart: boolean
    showlolan: boolean
}

export default class MainScreen extends React.Component<{}, State> {
    private hide: React.CSSProperties = {display: "none"};
    private messagesEnd: HTMLDivElement | null;
    constructor(props: {}) {
        super(props);
        this.state = {
            chatHistory: [],
            waiting: false,
            showHeart: false,
            showlolan: false

        };
        this.messagesEnd = null;
    }
    renderChats = () =>
        this.state.chatHistory.map(entry => {
            let classToUse = "";
            entry.type === "user" ? (classToUse = "user-input") : (classToUse = "receiver-input");
            return <p className={classToUse}>{entry.message}</p>;
        });

    onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        const {chatHistory} = this.state;
        event.preventDefault();
        console.log(event.currentTarget.chat.value);
        const newChatHistory = chatHistory.concat();
        let input = event.currentTarget.chat.value;
        newChatHistory.push({
            type: "user",
            message: event.currentTarget.chat.value,
        });
        this.setState(
            {
                chatHistory: newChatHistory,
            },
            () => setTimeout(async()=>{
                await this.getResponse(input)
            },2000)
        );

        console.log(newChatHistory);
        console.log(this.state.chatHistory);
        event.currentTarget.chat.value = "";
    };

    getResponse = async (userInput: string) => {
        const {chatHistory} = this.state;
        const res = await fetch(`http://localhost:9090/moon-bot/input?query=${userInput}`, {
            method: "POST",
            headers: {
                "Content-type": "application/json",
            },
        });
        const response = await res.json();
        console.log(response);
        const newChatHistory = chatHistory.concat();
        newChatHistory.push({
            type: "receiver",
            message: response.response,
        });
        this.setState({
            chatHistory: newChatHistory,
        });
        console.log(this.state.chatHistory);
    };

    scrollToBottom = () => {
        if (this.messagesEnd) {
            this.messagesEnd.scrollIntoView({behavior: "smooth"});
        }
    };

    

    componentDidUpdate() {
        this.scrollToBottom();
    }


    onHeartClick = () => {
        this.setState({showHeart: true});
        setTimeout(()=>{
            this.setState({showHeart: false});
        }, 3000)
    };


     onLolanClick = () => {
        this.setState({showlolan: true});
        setTimeout(()=>{
            this.setState({showlolan: false});
        }, 5000)
    };



    render() {
        const {showHeart, showlolan} = this.state;

        return (<React.Fragment>
            <div className="main-screen" >
                <img style={showHeart?undefined:this.hide} id="heart" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Love_Heart_SVG.svg/645px-Love_Heart_SVG.svg.png" />
                <img style={showlolan?undefined:this.hide} id="lolan" src="https://i2.kknews.cc/SIG=33hr3ia/ctp-vzntr/1537330766483o21po257o5.jpg" />
            </div>
            <ButtonBar onHeartclick={()=>this.onHeartClick()} onLolanClick={this.onLolanClick} />
            <div id="chat-box" className="chat-box" >
                {this.renderChats()}
            </div>
            <form onSubmit={this.onSubmit}>
                <TextField
                    label="Input"
                    margin="normal"
                    variant="outlined"
                    name="chat"
                    fullWidth
                />
            </form>
        </React.Fragment>);
    }
}
