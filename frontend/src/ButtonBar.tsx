import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { createStyles, Theme, withStyles, WithStyles, MuiThemeProvider } from '@material-ui/core/styles';
import './AppBar.css'
import { createMuiTheme } from '@material-ui/core/styles';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocalFloristIcon from '@material-ui/icons/LocalFlorist';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import CameraIcon from '@material-ui/icons/Camera';
import GpsNotFixedIcon from '@material-ui/icons/GpsNotFixed';


const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#3a3d4d'
        }
    }
});

const styles = (theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
        },
        grow: {
            flexGrow: 1,
        },
        menuButton: {
            marginLeft: -12,
            marginRight: 20,
        },
    });

interface Props extends WithStyles<typeof styles> {
    onHeartclick: ()=>void;
    onLolanClick: () => void;
}


class ButtonBar extends React.Component<Props> {
    render() {

        const { classes } = this.props;

        return (
            <MuiThemeProvider theme={theme}>
                <div className={classes.root}>
                    <AppBar position="static">
                        <Toolbar style={{
                            display: "flex",
                            justifyContent: "space-between",
                            alignItems: "center",
                        }}>
                            <IconButton className={classes.menuButton} color="inherit" onClick={this.props.onHeartclick} >
                                <FavoriteIcon />
                            </IconButton>
                            <IconButton className={classes.menuButton} color="inherit" onClick={this.props.onLolanClick} >
                                <LocalFloristIcon />
                            </IconButton>
                            <IconButton className={classes.menuButton} color="inherit" onClick={this.props.onLolanClick} >
                                <MusicNoteIcon />
                            </IconButton>
                            <IconButton className={classes.menuButton} color="inherit" onClick={this.props.onLolanClick}  >
                                <CameraIcon />
                            </IconButton>
                            <IconButton className={classes.menuButton} color="inherit" onClick={this.props.onLolanClick} >
                                <GpsNotFixedIcon />
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                </div>
            </MuiThemeProvider>
        )
    }
}

export default withStyles(styles)(ButtonBar);

