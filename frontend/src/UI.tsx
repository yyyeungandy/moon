import React from 'react';
import AppBar from "./AppBar";
import MenuDrawer from "./MenuDrawer";
import { CssBaseline } from '@material-ui/core';


interface State {
    menuDrawerOpen: boolean;
    infoDrawerOpen: boolean;
}

export default class UI extends React.Component<{}, State> {
    constructor(props:{}) {
        super(props);
        this.state = {
            menuDrawerOpen: false,
            infoDrawerOpen: false,
        }
    }

    render() {
        const {menuDrawerOpen, infoDrawerOpen} = this.state;
        return(
            <div>
                <CssBaseline/>
                <AppBar menuSwitch={()=>this.setState({menuDrawerOpen: !menuDrawerOpen})} />
                <MenuDrawer closeMenuDrawer={()=>this.setState({menuDrawerOpen: false})} openMenuDrawer={()=>{this.setState({menuDrawerOpen: false})}} menuDrawerOpen={menuDrawerOpen} />
            </div>
        )
    }
}