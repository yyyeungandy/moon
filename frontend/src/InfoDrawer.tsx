import React from 'react';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { withStyles, WithStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import InfoCard from './InfoCard';


const styles = {
    
};

interface Props extends WithStyles<typeof styles> {
    infoSwitch: () => void,
    infoOpen: boolean
}

class InfoDrawer extends React.Component<Props> {
   
    render() {
        return (
                <SwipeableDrawer
                    anchor="bottom"
                    open={this.props.infoOpen}
                    onClose={this.props.infoSwitch}
                    onOpen={this.props.infoSwitch}
                >
                    <InfoCard />
                </SwipeableDrawer>
        )
    }
}



export default (withStyles(styles)(InfoDrawer));

