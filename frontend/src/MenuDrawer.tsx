import React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import './MenuDrawer.css'

const styles = {
    list: {
        width: 200,
    },
    nested: {
        paddingLeft: 40,
    },
    shorterNested: {
        paddingLeft: 20,
    },
    shortestNested: {
        paddingLeft: 5,
    }
};

interface State {
}

interface Props extends WithStyles<typeof styles> {
    menuDrawerOpen: boolean;
    closeMenuDrawer: () => void;
    openMenuDrawer: () => void;
}

class MenuDrawer extends React.Component<Props, State> {

    constructor(props: Props){
        super(props);

    }

    private sideList = () => (
        <div className={this.props.classes.list}>
           side list
        </div>
        
    );

    render() {


        return (
            <div>
                <SwipeableDrawer
                    anchor="left"
                    open={this.props.menuDrawerOpen}
                    onClose={this.props.closeMenuDrawer}
                    onOpen={this.props.openMenuDrawer}
                >
                    {this.sideList()}
                </SwipeableDrawer>
            </div >
        )
    }
}



export default (withStyles(styles)(MenuDrawer));

