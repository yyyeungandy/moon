import React from 'react';
import { withStyles, WithStyles, Theme} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';

import './InfoCard.css'


const styles = (theme: Theme) => ({
  margin: {
    margin: theme.spacing.unit * 2,
  },
  card: {
    maxWidth: "100%",
  },
  media: {
    height: 100
  },
  info: {
    display: 'flex',
  }
});

interface IInfoCardProps extends WithStyles<typeof styles> {

}

class InfoCard extends React.Component<IInfoCardProps> {



  private getCarParkInfo = () => {
      return (
        <Card square={true}>
        Chat
        </Card>
      )
  }

  render() {
    return (
      <div>{this.getCarParkInfo()}</div>
    )
  }
}



export default (withStyles(styles)(InfoCard))
