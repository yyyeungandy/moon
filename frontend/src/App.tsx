import React, { Component, Fragment } from 'react';
import Main from './MainScreen';
import './App.css';
import UI from './UI';




class App extends Component {
  // componentDidMount = () => {
  //   document.addEventListener('touchmove',
  //     function (e) {
  //       e.preventDefault();
  //     }, { passive: false });
  // }
  render() {
    return (
        <Fragment>
            <UI />
            <Main />
        </Fragment>
    );
  }
}

export default App;
