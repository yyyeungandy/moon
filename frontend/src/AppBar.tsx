import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { createStyles, Theme, withStyles, WithStyles, MuiThemeProvider } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import './AppBar.css'
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#537380'
    }
  }
});

const styles = (theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    title: {
      [theme.breakpoints.up('sm')]: {
        display: 'block',
        width: '150px'
      },
    }
  });

interface Props extends WithStyles<typeof styles> {
  menuSwitch: () => void;

}


class TopAppBar extends React.Component<Props> {
  render() {

    const { classes } = this.props;

    return (
      <MuiThemeProvider theme={theme}>
        <div className={classes.root}>
          <AppBar position="static">
            <Toolbar>
              <IconButton className={classes.menuButton} color="inherit" onClick={this.props.menuSwitch}>
                <MenuIcon />
              </IconButton>
              <Typography className={classes.title} variant="inherit" noWrap>
                Space Monk
              </Typography>
            </Toolbar>
          </AppBar>
        </div>
      </MuiThemeProvider>
    )
  }
}

export default withStyles(styles)(TopAppBar);

