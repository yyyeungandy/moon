import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {

    return knex.schema.createTable("dayNightParks", (table) => {
        table.increments();
        table.integer("car_id").unique();
        table.foreign("car_id", "car.id");
        table.timestamps(false, true);

        table.enum("type", ["day-park", "night-park", "6-hours-park"]).notNullable();
        table.enum("weekdays", ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN", "PH"]).notNullable();
        table.boolean("excludePublicHoliday").notNullable();
        table.string("periodStart").notNullable();
        table.string("periodEnd").notNullable();
        table.enum("validUntil",["no-restrictions", "same-day", "following-day"]).notNullable();
        table.string("validUntilEnd")
        table.integer("price").notNullable();
        table.enum("covered",["covered", "semi-covered", "open-air", "mixed"]).notNullable()
        table.string("remark")

    })

};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("dayNightParks");
};