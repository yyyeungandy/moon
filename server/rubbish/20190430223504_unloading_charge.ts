import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {

    return knex.schema.createTable("unloadings", (table) => {
        table.increments();
        table.integer("car_id").unique();
        table.foreign("car_id", "car.id");
        table.timestamps(false, true);
        
        table.enum("type", ["hourly", "half-hourly"]).notNullable();
        table.integer("price").notNullable();
        table.json("usageThresholds"); //important t_t
        table.string("remark");
    })

};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("unloadings");
};