import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {

    return knex.schema.createTable("privileges", (table) => {
        table.increments();
        table.integer("car_id").unique();
        table.foreign("car_id", "car.id");
        table.timestamps(false, true);

        table.enum("weekdays", ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN", "PH"]).notNullable();
        table.boolean("excludePublicHoliday").notNullable();
        table.string("periodStart").notNullable();
        table.string("periodEnd").notNullable();
        table.string("description").notNullable();

    })

};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("privileges");
};