const dialogflow = require("dialogflow");
const uuid = require("uuid");
const express = require('express')


export class BotService{

    constructor(){
        this.sessionId = uuid.v4();
    }
    
    router(){
        const router = express.Router()
        router.post('/input', this.userInput)
        return router
    }

    userInput = async(req, res) => {
        console.log(req.query.query)
        let response = await runSample("ice-cream-helper-rlsjre", req.query.query, this.sessionId)
        res.json({response})

    }
}
/**
 * Send a query to the dialogflow agent, and return the query result.
 * @param {string} projectId The project to be used
 */


async function runSample(projectId = "ice-cream-helper-rlsjre", query, sessionId) {
    // A unique identifier for the given session
    const readline = require("readline").createInterface({
        input: process.stdin,
        output: process.stdout,
    });


    // Create a new session
    const sessionClient = new dialogflow.SessionsClient();
    const sessionPath = sessionClient.sessionPath(projectId, sessionId);

    // The text query request.
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                // The query to send to the dialogflow agent
                text: query,
                // The language used by the client (en-US)
                languageCode: "en-US",
            },
        },
    };

    // Send request and log result
    const responses = await sessionClient.detectIntent(request);
    console.log("Detected intent");
    const result = responses[0].queryResult;
    console.log(`  Query: ${result.queryText}`);
    console.log(`  Response: ${result.fulfillmentText}`);
    if (result.intent) {
        console.log(`  Intent: ${result.intent.displayName}`);
    } else {
        console.log(`  No intent matched.`);
    }
    return result.fulfillmentText
}


// runSample();