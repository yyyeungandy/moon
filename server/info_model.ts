import { IEnclosedElement } from "./model";

export interface Info {
    results: Result[];
}

export interface Result {
    park_Id: string;
    name: string;
    nature?: Nature;
    carpark_Type?: CarparkType;
    address?: Address;
    displayAddress: string;
    district?: string;
    latitude: number;
    longitude: number;
    contactNo: string;
    renditionUrls?: RenditionUrls;
    website: string;
    opening_status: OpeningStatus;
    openingHours?: OpeningHour[];
    gracePeriods?: GracePeriod[];
    heightLimits?: HeightLimit[];
    facilities?: Facility[];
    paymentMethods?: PaymentMethod[];
    privateCar?: IEnclosedElement;
    LGV?: IEnclosedElement;
    HGV?: IEnclosedElement;
    CV?: IEnclosedElement;
    coach?: IEnclosedElement;
    motorCycle?: IEnclosedElement;
    creationDate?: string;
    modifiedDate?: string;
    publishedDate?: string;
    lang: Lang;
}

enum Weekday {
    Fri = "FRI",
    Mon = "MON",
    Sat = "SAT",
    Sun = "SUN",
    Thu = "THU",
    Tue = "TUE",
    Wed = "WED",
    Ph = "PH",
}

interface OpeningHour {
    weekdays: Weekday[];
    excludePublicHoliday: boolean;
    periodStart: string;
    periodEnd: string;
    description?: string;
}
interface Address {
    buildingName?: string;
    streetName?: string;
    buildingNo?: string;
    floor?: string;
    subDistrict: string;
    dcDistrict: string;
    region: string;
    unitNo?: string;
    unitDescriptor?: string;
    blockNo?: string;
    blockDescriptor?: string;
    phase?: string;
    estateName?: string;
    villageName?: string;
}

enum CarparkType {
    MultiStorey = "multi-storey",
    OffStreet = "off-street",
}

enum Facility {
    Disabilities = "disabilities",
    EvCharger = "evCharger",
    Unloading = "unloading",
    Washing = "washing",
    ValetParking = "valet-parking"
}

interface GracePeriod {
    minutes: number;
    remark?: string;
}

interface HeightLimit {
    height: number;
    remark?: string;
}

enum Lang {
    EnUS = "en_US",
    ZhTW = "zh_TW",
    ZhCN = "zh_CN"
}



enum Nature {
    Commercial = "commercial",
    Government = "government",
}

enum OpeningStatus {
    Closed = "CLOSED",
    Open = "OPEN",
}

enum PaymentMethod {
    AutopayStation = "autopay-station",
    Cash = "cash",
    Master = "master",
    Octopus = "octopus",
    Visa = "visa",
    EPS = "EPS",
    Unionpay = "unionpay",
    Alipay = 'alipay',
}

interface RenditionUrls {
    square?: string;
    thumbnail?: string;
    banner?: string;
    carpark_photo?: string;
}

