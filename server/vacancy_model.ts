export interface Vacancy {
    results: Result[];
}

export interface Result {
    park_Id: string;
    privateCar?: Placeholder[];
    LGV?: Placeholder[];
    HGV?: Placeholder[];
    motorCycle?: Placeholder[];
    CV?: Placeholder[];
    coach?: Placeholder[];

}

export interface Placeholder {
    vacancy_type: VacancyType;
    vacancy: number;
    lastupdate: string;
    vacancyUNL?: number;
    vacancyEV?: number;
    vacancyDIS?: number;
    category?: Category;
}

export enum VacancyType {
    A = "A",
    B = "B",
    C = "C"
}



export enum Category {
    Hourly = "HOURLY",
    Daily = "DAILY",
    Monthly = "MONTHLY",
}