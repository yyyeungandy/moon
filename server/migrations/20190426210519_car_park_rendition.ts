import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
    return knex.schema.createTable("car_park_rendition", (table) => {
        table.increments();
        table.integer("car_park_id").unique()
        table.foreign("car_park_id", "car_park_info.id");
        table.string("square");
        table.string("thumbnail");
        table.string("banner");
        table.string("carpark_photo"); // main entrance
        table.timestamps(false, true);
    })
};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("car_park_rendition");
};