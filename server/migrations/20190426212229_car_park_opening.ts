import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
    return knex.schema.createTable("car_park_opening", (table) => {
        table.increments();
        table.integer("car_park_id").unique();
        table.foreign("car_park_id", "car_park_info.id");
        table.specificType("opening_hour","json array");
        table.timestamps(false, true);
    })
};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("car_park_opening");
};