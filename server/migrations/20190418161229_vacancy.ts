import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
    return knex.schema.createTable("car_park_vacancy", (table) => {
        table.increments();
        table.string("park_Id").notNullable();
        table.specificType("privateCar","json array");
        table.specificType("LGV","json array");
        table.specificType("HGV","json array");
        table.specificType("CV","json array");
        table.specificType("coach","json array");
        table.specificType("motorCycle","json array");
    })
};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("car_park_vacancy");
};