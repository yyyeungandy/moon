import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
    return knex.schema.createTable("car_park_cars", (table) => {
        table.increments();
        table.string("park_id").notNullable();
        table.enum("type", ["privateCar", "LGV", "HGV", "CV", "coach", "motorCycle"])
        table.integer("space").notNullable();
        table.integer("spaceDIS").notNullable();
        table.integer("spaceEV").notNullable();
        table.integer("spaceUNL").notNullable();
        table.specificType("hourlyCharges","json array");
        table.specificType("dayNightParks","json array");
        table.specificType("monthlyCharges","json array");
        table.specificType("privileges","json array");
        table.specificType("unloadings","json array");
        table.timestamps(false, true);
    })
};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("car_park_cars");
};